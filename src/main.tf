
terraform {
  required_providers {
    helm = {
      source  = "hashicorp/helm"
      version = "2.12.1"
    }
  }
}

locals {
  argo = {
    namespace = "argocd"
  }
}

variable "kubernetes_config_path" {
  default = "~/.kube/config"
}

provider "helm" {
  # TODO: configure properly connection,
  # when cloud deployment is up and running

  kubernetes {
    config_path = var.kubernetes_config_path
  }
}

resource "helm_release" "install_argo" {
  name = "argocd"

  repository       = "https://argoproj.github.io/argo-helm"
  chart            = "argo-cd"
  namespace        = local.argo.namespace
  create_namespace = true

  # helm repo add argocd https://argoproj.github.io/argo-helm
  # helm repo list
  version = "5.53.12"
  values  = [file("./argo-helm-values.yaml")]

  timeout = 20 * 60
}

variable "argocd_gitops_repo_private_key_file" {
  sensitive = true
}

resource "helm_release" "argocd_deploy_gitops" {
  depends_on = [helm_release.install_argo]
  lifecycle {
    replace_triggered_by = [helm_release.install_argo]
  }

  name      = "argocd-gitops"
  namespace = local.argo.namespace
  chart     = "./argo-init-infra-gitops"

  set {
    name  = "ssh_repo"
    value = "git@gitlab.com:serost-kubernetes-app/infrastructure-gitops.git"
  }

  set {
    name  = "ssh_private_key"
    value = file(var.argocd_gitops_repo_private_key_file) # no need to remove newlines
  }

}
